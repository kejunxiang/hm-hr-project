import axios from 'axios'
import { Message } from 'element-ui'
import store from '@/store/index'
import router from '@/router/index'
// create an axios instance
const service = axios.create({
  // 环境变量检查process.env
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})
// 添加请求拦截器
service.interceptors.request.use(function(config) {
  // 在发送请求之前做些什么
  if (store.getters.token) {
    // 判断登录是否超时
    const timeout = 1000 * 60 * 60 * 2
    const time = Date.now() - localStorage.getItem('timemap')
    if (time > timeout) {
      store.dispatch('user/logout')
      router.push('/login')
      return Promise.reject(new Error('登录超时'))
    }
    config.headers.Authorization = 'Bearer ' + store.getters.token
  }
  return config
}, function(error) {
  // 对请求错误做些什么
  return Promise.reject(error)
})

// 添加响应拦截器
service.interceptors.response.use(function(response) {
  // console.log(response)
  if (response.data.success) {
    return response.data.data
  } else {
    Message.error(response.data.message)
    return Promise.reject(new Error(response.data.message))
  }
  // 对响应数据做点什么
}, function(error) {
  // 对响应错误做点什么
  // console.dir(error)
  // token错误被动介入
  if (error.response && error.response.data.code === 10002) {
    store.dispatch('user/logout')
    router.push('/login')
  }
  Message.error(error.message)
  return Promise.reject(error)
})

export default service
