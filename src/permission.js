import router from '@/router'
import store from '@/store'

router.beforeEach(async(to, from, next) => {
  const token = store.getters.token
  const whiteList = ['/login', '/404', '/test']
  const url = to.path
  // 如果有token去的是登录页跳转首页
  if (token && url === '/login') {
    next('/')
  }
  // 如果有token去的不是登录页
  if (token && url !== '/login') {
    // 写在导航守卫因为不止首页需要用到用户数据
    // 当进入页面判断是否已经有数据,没有发请求获取数据
    if (!store.state.user.userInfo.username) {
      await store.dispatch('user/getUserInfo')
      // 拿完数据之后, 进入页面之前, 就是触发路由权限筛选的位置
      const routes = await store.dispatch('permission/filterRoutes', store.state.user.userInfo.roles.menus)
      // console.log(routes)
      router.addRoutes([...routes, { path: '*', redirect: '/404', hidden: true }])
      next(to.path)
      return
    }
    next()
  }
  // 如果没有token去的是白名单
  if (!token && whiteList.includes(url)) {
    next()
  }
  // 如果没有token去的不是白名单
  if (!token && !whiteList.includes(url)) {
    next('/login')
  }
  // if (token) {
  //   if (url === '/login') {
  //     next('/')
  //   } else {
  //     // 写在导航守卫因为不止首页需要用到用户数据
  //     // 当进入页面判断是否已经有数据,没有发请求获取数据
  //     if (!store.state.user.userInfo.username) {
  //       await store.dispatch('user/getUserInfo')
  //       // 拿完数据之后, 进入页面之前, 就是触发路由权限筛选的位置
  //       const routes = await store.dispatch('permission/filterRoutes', store.state.user.userInfo.roles.menus)
  //       // console.log(routes)
  //       router.addRoutes([...routes, { path: '*', redirect: '/404', hidden: true }])
  //       next(to.path)
  //       return
  //     }
  //     next()
  //   }
  // } else {
  //   if (whiteList.includes(url)) {
  //     next()
  //   } else {
  //     next('/login')
  //   }
  // }
})

