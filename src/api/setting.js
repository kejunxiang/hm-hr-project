import request from '@/utils/request'

// 获取公司信息
export function getCompanyInfo(id) {
  return request({
    url: `/company/${id}`
  })
}
// 获取角色列表
export function getrRoleList(params) {
  return request({
    url: '/sys/role',
    params
  })
}
// 新增角色
export function addRole(data) {
  return request({
    url: '/sys/role',
    method: 'post',
    data
  })
}
// 根据id获取角色详情
export function getrRole(id) {
  return request({
    url: `/sys/role/${id}`
  })
}

// 编辑角色
export function editRole(data) {
  return request({
    url: `/sys/role/${data.id}`,
    method: 'PUT',
    data
  })
}

// 删除角色
export function delRole(id) {
  return request({
    url: `/sys/role/${id}`,
    method: 'delete'
  })
}

// 设置角色权限
export function assignPerm(data) {
  return request({
    url: '/sys/role/assignPrem',
    method: 'put',
    data
  })
}
