import request from '@/utils/request'

// 获取所有权限
export function getPermission() {
  return request({
    url: '/sys/permission'
  })
}
// 根据ID获取权限点详情
export function getPermDetails(id) {
  return request({
    url: `/sys/permission/${id}`
  })
}
// 根据ID更新权限点详情

export function upDatePermDetails(data) {
  return request({
    url: `/sys/permission/${data.id}`,
    method: 'put',
    data
  })
}
// 根据id删除权限点
export function delPerm(id) {
  return request({
    url: `/sys/permission/${id}`,
    method: 'DELETE'
  })
}

// 新增权限点
export function addPerm(data) {
  return request({
    url: `/sys/permission`,
    method: 'post',
    data
  })
}

