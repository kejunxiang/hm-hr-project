import request from '@/utils/request'

// 获取所有部门信息
export function getdeplList() {
  return request({
    url: '/company/department'
  })
}

// 获取员工简单信息
export function getEmployeeInfo() {
  return request({
    url: '/sys/user/simple'
  })
}

// 新增部门
export function addDepl(data) {
  return request({
    url: '/company/department',
    method: 'post',
    data
  })
}
// 根据id查询部门详情
export function getdeptDetails(id) {
  return request({
    url: `/company/department/${id}`
  })
}

// 根据id修改部门详情
export function editdeptDetails(data) {
  return request({
    url: `/company/department/${data.id}`,
    method: 'put',
    data
  })
}
// 根据id删除部门
export function deldeptDetails(id) {
  return request({
    url: `/company/department/${id}`,
    method: 'DELETE'
  })
}

