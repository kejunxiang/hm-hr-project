import { asyncRoutes, constantRoutes } from '@/router/index'

const state = {
  routesList: []
}

const mutations = {
  setroutesList(store, data) {
    store.routesList = [...constantRoutes, ...data]
  }

}

const actions = {
  filterRoutes(store, menus) {
    // 这个函数负责过滤路由, 需要传入当前用户的权限列表 menus
    const routes = asyncRoutes.filter(route => menus.includes(route.name))
    // console.log('筛选后的路由数据', routes)
    store.commit('setroutesList', routes)
    return routes
  }

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

