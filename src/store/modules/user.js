import { login, getUserInfo, getStaffInfo } from '@/api/user'
// import Cookie from 'js-cookie'
const state = {
  userInfo: {},
  token: localStorage.getItem('72_hr_token') || ''
  // token: Cookie.get('72_hr_token') || ''

}
const mutations = {
  setToken(state, token) {
    state.token = token
    localStorage.setItem('72_hr_token', token)
    localStorage.setItem('timemap', Date.now())
    // Cookie.set('72_hr_token', token)
  },
  setUserInfo(state, info) {
    state.userInfo = info
  },
  removeToken() {
    state.token = ''
    localStorage.removeItem('72_hr_token')
  },
  removeUserInfo() {
    state.userInfo = {}
  }
}
const actions = {
  async login(store, loginForm) {
    const res = await login(loginForm)
    store.commit('setToken', res)
  },
  async getUserInfo(store) {
    const res = await getUserInfo()
    // console.log(res)
    const staffInfo = await getStaffInfo(res.userId)
    // console.log(staffInfo)
    store.commit('setUserInfo', { ...res, ...staffInfo })
  },
  logout(store) {
    store.commit('removeToken')
    store.commit('removeUserInfo')
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions }
