import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// import locale from 'element-ui/lib/locale/lang/en' // lang i18nue

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import '@/icons' // icon
import '@/permission' // permission control

import myPlugins from '@/components/myPlugins'
// 引入混入资源
import myMixIn from './mixin'
// 插入页面
Vue.mixin(myMixIn)

Vue.use(myPlugins)

import { i18n } from '@/i18n/index'
Vue.use(ElementUI, {
  i18n: (key, value) => i18n.t(key, value)
})
// set ElementUI lang to EN
// Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)

Vue.directive('fiximg', {
  inserted(el) {
    // console.log('插入页面了')
    // console.log(el)
    el.onerror = () => {
      // 自定义在线图片
      // el.src = 'https://ts1.cn.mm.bing.net/th?id=OIP-C.FAskO3RwnhUtQnUjtQgP8wHaHa&w=158&h=170&c=8&rs=1&qlt=90&o=6&dpr=1.25&pid=3.1&rm=2'
      // 也可以引入为变量
      // import img from '@/assets/common/head.jpg'
      // el.src = img
      el.src = require('@/assets/common/head.jpg')
    }
    // 图片为空情况
    // el.src = el.src || 'http://www.baidu.com.img.hahaha'
    el.src = el.src || require('@/assets/common/head.jpg')
  },
  update(el) {
    el.src = el.src || require('@/assets/common/head.jpg')
  }
})
// 全局注册单个过滤器
// import { formatDate } from '@/filters/index'
// // 定义全局过滤器处理时间格式
// Vue.filter('formdata', formatDate
// )

// 注册多个过滤器
// 全部引入遍历注册过滤器
import * as myFilters from '@/filters/index'
for (const key in myFilters) {
  Vue.filter(key, myFilters[key])
}

Vue.config.productionTip = false

new Vue({
  i18n,
  el: '#app',
  router,
  store,
  render: h => h(App)
})
