import Uploadexcel from '@/components/Uploadexcel'
import toolbar from '@/components/toolbar'
import imgUpload from '@/components/imgUpload'
import screenFull from '@/components/screenFull'
import ThemePicker from '@/components/ThemePicker'
import lang from '@/components/lang'
export default {
  install(Vue) {
    // console.log('安装插件')
    Vue.component('toolbar', toolbar)
    Vue.component('Uploadexcel', Uploadexcel)
    Vue.component('imgUpload', imgUpload)
    Vue.component('screenFull', screenFull)
    Vue.component('ThemePicker', ThemePicker)
    Vue.component('lang', lang)
  }
}
