import Layout from '@/layout'

export default {
  name: 'social_securitys',
  path: '/social',
  component: Layout,
  children: [{
    path: '',
    component: () => import('@/views/social/index'),
    meta: { title: '社保', icon: 'table' }
  }]
}
