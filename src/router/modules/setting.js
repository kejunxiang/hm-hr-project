import Layout from '@/layout'

export default {
  name: 'settings',
  path: '/setting',
  component: Layout,
  children: [{
    path: '',
    component: () => import('@/views/setting/index'),
    meta: { title: '设置', icon: 'setting' }
  }]
}

