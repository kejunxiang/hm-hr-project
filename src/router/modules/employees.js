import Layout from '@/layout'

export default {
  name: 'employees',
  path: '/employees',
  component: Layout,
  children: [
    {
      path: '',
      component: () => import('@/views/employees/index'),
      meta: { title: '员工管理', icon: 'people' }
    },
    {
      path: 'import',
      component: () => import('@/views/employees/import.vue'),
      meta: { title: '导入', icon: 'people' },
      hidden: true
    },
    {
      path: 'detail/:id',
      component: () => import('@/views/employees/detail.vue'),
      meta: { title: '员工详情', icon: 'people' },
      hidden: true
    },
    {
      path: 'print/:id',
      component: () => import('@/views/employees/print.vue'),
      meta: { title: '打印详情', icon: 'people' },
      hidden: true
    }
  ]
}

