export default {
  methods: {
    permissionManage(permissionName) {
      // console.log(this.$store.state.user.userInfo.roles.points)
      // const result = this.$store.state.user.userInfo.roles
      // const points = result ? result.points : []

      // return points.includes(permissionName)
      // ?.可选链如果前面值不存在返回undefined,不会导致报错
      // ?? 表示空值合并运算符，特殊情况替代短路运算，可以返回 0 false ‘’等值，不会进行隐式转换
      const points = this.$store.state.user.userInfo.roles?.points || []
      return points.includes(permissionName)
    }
  }
}
