import Vue from 'vue'
import Vuei18n from 'vue-i18n'
import enLocale from 'element-ui/lib/locale/lang/en'
import zhLocale from 'element-ui/lib/locale/lang/zh-CN'
import zhRoute from '@/i18n/zh'
import enRoute from '@/i18n/en'
Vue.use(Vuei18n)
export const i18n = new Vuei18n({
  locale: localStorage.getItem('lang') || 'zh',
  messages: {
    en: {
      ...enRoute,
      ...enLocale,
      'zaoan': 'good morning'
    },
    zh: {
      ...zhRoute,
      ...zhLocale,
      'zaoan': '早上好'
    }
  }
})
